﻿using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoProgress_Controll : MonoBehaviour, IMixedRealityPointerHandler
{
    [SerializeField] private UnityEngine.Video.VideoPlayer vp;
    private PinchSlider slider;
    bool slide = false;

    
    private void Awake()
    {
        slider = GetComponent<PinchSlider>();
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        slide = true; 
    }
  
    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        float frame = (float)slider.SliderValue * (float)vp.frameCount;
        vp.frame = (long)frame;
        slide = false;
    }

    void Update()
    {
        if (!slide)
        {
            slider.SliderValue = (float)vp.frame / (float)vp.frameCount;
        }
    }


    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
    }
    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
    }
   

}
