﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Dialog_Controller : MonoBehaviour
{

    private int counter = 1;
    [SerializeField] private GameObject ImagePanel = null;
    [SerializeField] private GameObject Subtitle = null;
    [SerializeField] private GameObject Body = null;
    [SerializeField] private GameObject SideNumber = null;
    private string sub = "Subtitle";
    private string body = "Body";
    private string number = "/9";


    public void forwardBtn()
    {
        if(counter < 9)
        {
            counter++;
            controllDialog(counter);
        }

    }

    public void backwardsBtn()
    {
        if(counter > 1)
        {
            counter--;

            if (counter == 1)
            {
                Subtitle.GetComponent<TextMeshProUGUI>().text = sub;
                Body.GetComponent<TextMeshProUGUI>().text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea";
                SideNumber.GetComponent<TextMeshProUGUI>().text = "1/9";
            }
            else
            {
                
                controllDialog(counter);
            } 
        }
    }

    public void controllDialog(int counter)
    {

         Subtitle.GetComponent<TextMeshProUGUI>().text = sub + " " + counter.ToString();
         Body.GetComponent<TextMeshProUGUI>().text = body + " " + counter.ToString();
         SideNumber.GetComponent<TextMeshProUGUI>().text = counter.ToString() + number;

    }




}
