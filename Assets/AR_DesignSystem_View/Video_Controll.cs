﻿using Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking;
using Microsoft.MixedReality.Toolkit.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Video_Controll : MonoBehaviour
{
    [SerializeField] private GameObject Video = null;
    [SerializeField] private GameObject PlayPauseBtnImage = null;
    [SerializeField] private Sprite PlaySprite = null;
    [SerializeField] private Sprite PauseSprite = null;
    [SerializeField] private GameObject Background = null;
    [SerializeField] private Sprite LockSprite = null;
    [SerializeField] private Sprite UnlockSprite = null;



    private UnityEngine.Video.VideoPlayer vp;
    private RawImage thumbnail;
    private bool vidActive = false;
    private FollowEyeGazeGazeProvider Gazeprovider;
    private Image cBThumbnail;


    private void Awake()
    {
        vp = GetComponent<UnityEngine.Video.VideoPlayer>();
        thumbnail = GetComponent<RawImage>();
        cBThumbnail = Background.GetComponent<Image>();
    }

    public void controllVideo()
    {

        if (vp.isPlaying)
        {

            vp.Stop();
            thumbnail.enabled = true;
            PlayPauseBtnImage.GetComponent<Image>().sprite = PlaySprite;
        }
        else
        {
            vp.Play();
            thumbnail.enabled = false;
            PlayPauseBtnImage.GetComponent<Image>().sprite = PauseSprite;
        }
    }

    public void popUp()
    {
        if (!Video.active)
        {
            Video.SetActive(true);
            vidActive = true;
        }
        else if(Video.active)
        {
            Video.SetActive(false);
            vidActive = false;
        }
    }

    public void gazeControl()
    {
        Gazeprovider = Video.GetComponent<FollowEyeGazeGazeProvider>();

        if (Gazeprovider.isActiveAndEnabled)
        {
            Gazeprovider.enabled = false;
            cBThumbnail.sprite = LockSprite;
        }
        else if (!Gazeprovider.isActiveAndEnabled)
        {
            Gazeprovider.enabled = true;
            cBThumbnail.sprite = UnlockSprite;


        }

    }
}
