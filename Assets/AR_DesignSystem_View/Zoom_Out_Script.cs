﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Zoom_Out_Script : MonoBehaviour
{

    [SerializeField] private GameObject Scene = null;

    public void ZoomMethod()
    {

        if (this.gameObject.GetComponent<Toggle>().isOn)
        {
            Scene.transform.position = new Vector3(0, 0, 4.7f);
        }
        else
        {
            Scene.transform.position = new Vector3(0, 0, 3.7f);
        }

    }


}
