﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] private GameObject Camera = null;
    private Vector3 target;

    // Update is called once per frame
    void Update()
    {
        target = new Vector3(Camera.transform.position.x, 
                             0, 
                             0);
        transform.LookAt(target);
        transform.Rotate(0, 180, 0);
        
    }
}
