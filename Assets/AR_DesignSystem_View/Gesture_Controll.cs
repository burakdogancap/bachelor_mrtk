﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gesture_Controll : MonoBehaviour
{

    public void Awake()
    {
        this.gameObject.SetActive(false);
    }

    public void Update()
    {

        if (this.gameObject.active)
        {
            Vector3 gazePos = GameObject.Find("DefaultCursor(Clone)").transform.position;
            this.gameObject.transform.position = gazePos;
        }
    }
}
