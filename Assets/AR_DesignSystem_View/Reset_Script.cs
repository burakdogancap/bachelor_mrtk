﻿using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Reset_Script : MonoBehaviour
{

    [SerializeField] private GameObject Hcon1 = null;
    [SerializeField] private GameObject Hcon2 = null;
    [SerializeField] private GameObject Hcon3 = null;

    [SerializeField] private GameObject CB1 = null;
    [SerializeField] private GameObject CB2 = null;

    [SerializeField] private GameObject RB1 = null;
    [SerializeField] private GameObject RB2 = null;
    [SerializeField] private GameObject RB3 = null;

    [SerializeField] private GameObject DD1 = null;
    [SerializeField] private GameObject DD2 = null;
    [SerializeField] private GameObject DD3 = null;







    public void Reset()
    {
        if (this.gameObject.GetComponent<Toggle>().isOn)
        {

            Hcon1.transform.localPosition = new Vector3( -2302 , 290, -802);
            Hcon1.transform.rotation = Quaternion.Euler(0, -30 , 0);
            CB1.GetComponent<Toggle>().isOn = false;
            CB2.GetComponent<Toggle>().isOn = false;
            disableDragDrop(Hcon1);
            closeSidePanel(Hcon1);

            Hcon2.transform.localPosition = new Vector3(-154, 290, -6);
            Hcon2.transform.rotation = Quaternion.Euler(0, 0, 0);
            RB1.GetComponent<Toggle>().isOn = true;
            RB2.GetComponent<Toggle>().isOn = false;
            RB3.GetComponent<Toggle>().isOn = false;
            disableDragDrop(Hcon2);
            closeSidePanel(Hcon2);


            Hcon3.transform.localPosition = new Vector3(2174, 290, -532.9f);
            Hcon3.transform.rotation = Quaternion.Euler(0, 30, 0);
            disableDragDrop(Hcon3);
            closeSidePanel(Hcon3);


        }
    }

    public void disableDragDrop(GameObject container)
    {
        if (container.GetComponent<ManipulationHandler>().enabled)
        {
            switch (container.name)
            {
                case "Hilfscontainer1": DD1.GetComponent<Button>().onClick.Invoke();
                    break;

                case "Hilfscontainer2": DD2.GetComponent<Button>().onClick.Invoke();
                    break;

                case "Hilfscontainer3": DD3.GetComponent<Button>().onClick.Invoke();
                    break;
            }
        }
    }

    public void closeSidePanel(GameObject container)
    {
        if (container.transform.Find("SidePanel"))
        {
            container.transform.Find("SidePanel").gameObject.SetActive(false);
        }
       
    }
}
