﻿using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class DragDropBtn : MonoBehaviour
{
    [SerializeField] private GameObject Hilfscontainer = null;
   
    int counter = 0;

    public void DragDropFunction()
    {
            counter++;

            if (counter % 2 == 1 && Hilfscontainer.GetComponent<BoxCollider>().enabled == false)
            {
                Hilfscontainer.GetComponent<BoxCollider>().enabled = true;
                Hilfscontainer.GetComponent<ManipulationHandler>().enabled = true;
                Hilfscontainer.GetComponent<BoundingBox>().enabled = true;

            }
            else
            {
                Hilfscontainer.GetComponent<BoxCollider>().enabled = false;
                Hilfscontainer.GetComponent<ManipulationHandler>().enabled = false;
                Hilfscontainer.GetComponent<BoundingBox>().enabled = false;
           }
        }
     


}
