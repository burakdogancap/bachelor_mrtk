﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IFeld_Controller : MonoBehaviour
{
    
    public void IFeldController()
    {
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
        }
    }
}
