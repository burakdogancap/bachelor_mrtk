﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caplogo_Materials : MonoBehaviour
{

    [SerializeField] private Material mblue= null;
    [SerializeField] private Material mblue2 = null;
    [SerializeField] private Material mgreen = null;
    [SerializeField] private Material mgreen2 = null;
    [SerializeField] private Material mred = null;
    [SerializeField] private Material mred2 = null;


    private Renderer rend;


    public void Start()
    {
        rend = GetComponent<Renderer>();
        blueMaterial();
    }

    public void blueMaterial()
    {

        rend.materials[0].color = mblue2.color;//(18, 170, 219)
        rend.materials[1].color = mblue.color;//(1, 111, 172)
        rend.materials[2].color = mblue2.color;//(18, 170, 219)
    }


    public void redMaterial()
    {
        rend.materials[0].color = mred2.color;//(255, 48, 76)
        rend.materials[1].color = mred.color;//(179, 34, 53)
        rend.materials[2].color = mred2.color;//(255, 48, 76)
      
    }

    public void greenMaterial()
    {
        rend.materials[0].color = mgreen2.color;//(149, 230, 22)
        rend.materials[1].color = mgreen.color;//(100, 153, 15)
        rend.materials[2].color = mgreen2.color;//(149, 230, 22)
        
    }


}
