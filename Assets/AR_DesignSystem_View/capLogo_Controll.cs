﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class capLogo_Controll : MonoBehaviour
{
    bool left = false, right = false;
    private float endRotation,startRotation;

    [SerializeField] private GameObject ButtonLeft = null;
    [SerializeField] private GameObject ButtonRight = null;

    public void rotateLeft()
    {
        StartCoroutine(Rotate(3.0f, "left"));
        ButtonRight.GetComponent<Button>().enabled = false;
    }

    public void rotateRight()
    {
        StartCoroutine(Rotate(3.0f, "right"));
        ButtonLeft.GetComponent<Button>().enabled = false;
    }

    IEnumerator Rotate(float duration, string direction)
    {
       startRotation = transform.eulerAngles.y;
       
        if (direction.Equals("left")){
            endRotation = startRotation + 360.0f;
        }
        else if (direction.Equals("right"))
        {
            endRotation = startRotation - 360.0f;
        }
       
        float t = 0.0f;
        while (t < duration)
        {
            t += Time.deltaTime;
            float yRotation = Mathf.Lerp(startRotation, endRotation, t / duration) % 360.0f;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, yRotation, transform.eulerAngles.z);
            yield return null;
        }
        ButtonRight.GetComponent<Button>().enabled = true;
        ButtonLeft.GetComponent<Button>().enabled = true;
    }

}