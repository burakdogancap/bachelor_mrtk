﻿using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDropSpeech : MonoBehaviour
{
    [SerializeField] private GameObject Hilfscontainer = null;

    public void speechDrag()
    {
        Hilfscontainer.GetComponent<BoxCollider>().enabled = true;
        Hilfscontainer.GetComponent<ManipulationHandler>().enabled = true;
        Hilfscontainer.GetComponent<BoundingBox>().enabled = true;
    }

    public void speechDrop()
    {
        Hilfscontainer.GetComponent<BoxCollider>().enabled = false;
        Hilfscontainer.GetComponent<ManipulationHandler>().enabled = false;
        Hilfscontainer.GetComponent<BoundingBox>().enabled = false;
    }
}
