﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Experimental.U2D;
using UnityEngine.Experimental.Rendering;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Shadow))]
public class AR_DesignSystem_Button : AR_DesignSystem
{

    //Elements to style
    Image image;
    Button button;
    TextMeshProUGUI text;
    Font font;
    Font fontSize;

    //Component Type Options
    public enum ButtonType
    {
        //Types of Buttons
        Primary_Btn,
        Secondary_Btn,
        Tertiary_Btn,
        Decline,
        Warning,
        Round
    }
    

    public ButtonType buttonType;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        button = GetComponent<Button>();
        text = GetComponentInChildren<TextMeshProUGUI>();
        image = GetComponent<Image>();


        button.transition = Selectable.Transition.SpriteSwap;
        button.targetGraphic = image;

        image.type = Image.Type.Sliced;

        switch(buttonType)
        {
            case ButtonType.Primary_Btn:
                image.sprite = skinData.buttonSprite;
                button.spriteState = skinData.buttonSpriteState;
                image.color = skinData.primaryColor;
                text.color = skinData.whiteColor;
                break;
            case ButtonType.Secondary_Btn:
                image.sprite = skinData.buttonSprite;
                button.spriteState = skinData.buttonSpriteState;
                image.color = skinData.whiteColor;
                text.color = skinData.primaryColor;
                break;
            case ButtonType.Tertiary_Btn:
                image.sprite = skinData.buttonSprite;
                button.spriteState = skinData.buttonSpriteState;
                image.color = skinData.successColor;
                text.color = skinData.whiteColor;
                break;
            case ButtonType.Decline:
                image.sprite = skinData.buttonSprite;
                button.spriteState = skinData.buttonSpriteState;
                image.color = skinData.errorColor;
                text.color = skinData.whiteColor;
                break;
            case ButtonType.Warning:
                image.sprite = skinData.buttonSprite;
                button.spriteState = skinData.buttonSpriteState;
                image.color = skinData.warningColor;
                text.color = skinData.darkgreyColor;
                break;
            case ButtonType.Round:
                image.sprite = skinData.buttonRoundSprite;
                image.color = skinData.primaryColor;

                if (button.tag.Equals("Title"))
                {
                    setColorForColorPicker();
                   
                }

                break;
        }
    }
    public void setColorForColorPicker()
    {

        switch (button.name)
        {
            case "CapBlue":
                image.color = skinData.capgeminiBlue;
                break;
            case "VibBlue":
                image.color = skinData.vibrantBlue;
                break;
            case "DeepPur":
                image.color = skinData.deepPurple;
                break;
            case "TechRed":
                image.color = skinData.techRed;
                break;
            case "ZestGre":
                image.color = skinData.zestGreen;
                break;
        }
            
     
        

    }
}

