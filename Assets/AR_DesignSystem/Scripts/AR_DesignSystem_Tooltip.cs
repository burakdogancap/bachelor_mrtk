﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Experimental.U2D;
using UnityEngine.Experimental.Rendering;

public class AR_DesignSystem_Tooltip : AR_DesignSystem
{

    [SerializeField] private GameObject TooltipBackground = null;
    [SerializeField] private GameObject TooltipText = null;
    [SerializeField] private GameObject TooltipArrow = null;

    public void Awake()
    {
        //this.gameObject.SetActive(false);
    }

    public enum TooltipType
    {
        //Types of Tooltips
        Default,
        DefaultAlt
    }

    Image tooltipBackground;
    Image tooltipArrow;
    TextMeshProUGUI text;



    public TooltipType tooltipType;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        tooltipBackground = TooltipBackground.GetComponent<Image>();
        tooltipArrow = TooltipArrow.GetComponent<Image>();
        text = TooltipText.GetComponent<TextMeshProUGUI>();


        switch (tooltipType)
        {
            case TooltipType.Default:
                tooltipBackground.color = skinData.primaryColor;
                tooltipArrow.color = skinData.primaryColor;
                text.color = skinData.whiteColor;
                break;
            case TooltipType.DefaultAlt:
                tooltipBackground.color = skinData.whiteColor;
                tooltipArrow.color = skinData.whiteColor;
                text.color = skinData.primaryColor;
                break;
        }
    }
}
