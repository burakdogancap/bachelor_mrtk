﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Experimental.U2D;
using UnityEngine.Experimental.Rendering;

[RequireComponent(typeof(Toggle))]
public class AR_Design_SelectionControlCheckbox : AR_DesignSystem
{
    [SerializeField] private GameObject Image = null;
   

    public enum SelectionControlCheckboxType
    {
        //Types of SelectionControls for Checkbox
        Default,
        Confirm,
        Decline,
        Warning
    }
  
    Image image;
    TextMeshProUGUI text;
 

    public SelectionControlCheckboxType selectionControllType;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

       
        image = Image.GetComponent<Image>();

        switch (selectionControllType)
        {
            case SelectionControlCheckboxType.Default:
                image.color = skinData.primaryColor;
                break;
            case SelectionControlCheckboxType.Confirm:
                image.color = skinData.successColor;
                break;
            case SelectionControlCheckboxType.Decline:
                image.color = skinData.errorColor;
                break;
            case SelectionControlCheckboxType.Warning:
                image.color = skinData.warningColor;
                break;
        }
    }


}
