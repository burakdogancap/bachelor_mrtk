﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Experimental.U2D;
using UnityEngine.Experimental.Rendering;

[RequireComponent(typeof(Slider))]
public class AR_Design_SelectionControlSwitch : AR_DesignSystem
{
    [SerializeField] private GameObject Image = null;
    [SerializeField] private GameObject Knob = null;
    [SerializeField] private GameObject FillImage = null;

    public enum SelectionControlSwitchType
    {
        //Types of SelectionControls for Switch
        Default,
        Confirm,
        Decline,
        Warning
    }

    Image image;
    Image knob;
    Image fillArea;

    public SelectionControlSwitchType selectionControllType;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        image = Image.GetComponent<Image>();
        knob = Knob.GetComponent<Image>();
        fillArea = FillImage.GetComponent<Image>();

        switch (selectionControllType)
        {
            case SelectionControlSwitchType.Default:
                image.color = skinData.primaryColor_3;
                knob.color = skinData.primaryColor;
                fillArea.color = skinData.primaryColor_3;
                break;
            case SelectionControlSwitchType.Confirm:
                image.color = skinData.successColor;
                knob.color = skinData.successColor;
                break;
            case SelectionControlSwitchType.Decline:
                image.color = skinData.errorColor;
                knob.color = skinData.errorColor;
                break;
            case SelectionControlSwitchType.Warning:
                image.color = skinData.warningColor;
                knob.color = skinData.warningColor;
                break;
        }
    }
}


