﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Experimental.U2D;
using UnityEngine.Experimental.Rendering;

public class AR_DesignSystemCard : AR_DesignSystem
{

    [SerializeField] private GameObject AlertTitlePanel = null;
    [SerializeField] private GameObject AlertTitle = null;
    [SerializeField] private GameObject AlertIcon = null;
    [SerializeField] private GameObject AlertDesc = null;
    [SerializeField] private GameObject AlertButton = null;
    [SerializeField] private GameObject AlertButtonText = null;


    public enum AlertType
    {
        //Types of Alerts
        Confirm,
        Decline,
        Warning
    }

    Image alertTitlePanel;
    TextMeshProUGUI alertTitle;
    //SVGImage alertIcon;
    TextMeshProUGUI alertDesc;
    Image buttonBackground;
    TextMeshProUGUI buttonText;

    public AlertType alertType;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        alertTitlePanel = AlertTitlePanel.GetComponent<Image>();
        alertTitle = AlertTitle.GetComponent<TextMeshProUGUI>();
        //alertIcon = AlertIcon.GetComponent<SVGImage>();
        alertDesc = AlertDesc.GetComponent<TextMeshProUGUI>();
        buttonBackground = AlertButton.GetComponent<Image>();
        buttonText = AlertButtonText.GetComponent<TextMeshProUGUI>();

        //SetUp that doesn't change
        alertTitle.color = skinData.darkgreyColor;
        alertDesc.color = skinData.darkgreyColor;

        switch (alertType)
        {
            case AlertType.Confirm:
                alertTitlePanel.color = skinData.successColor;
                //alertIcon.color = skinData.darkgreyColor;
                buttonBackground.color = skinData.successColor;
                buttonText.color = skinData.whiteColor;
                break;
            case AlertType.Decline:
                alertTitlePanel.color = skinData.errorColor;
                //alertIcon.sprite = skinData.declineSprite;
                buttonBackground.color = skinData.errorColor;
                buttonText.color = skinData.whiteColor;
                break;
            case AlertType.Warning:
                alertTitlePanel.color = skinData.warningColor;
                //alertIcon.sprite = skinData.warningSprite;
                buttonBackground.color = skinData.warningColor;
                buttonText.color = skinData.darkgreyColor;
                break;

        }
    }
}
