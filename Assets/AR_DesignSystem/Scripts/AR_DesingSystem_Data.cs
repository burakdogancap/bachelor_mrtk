﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[CreateAssetMenu(fileName = "new Skin", menuName = "AR DesigSystem Data")]
public class AR_DesingSystem_Data : ScriptableObject
{

    [Header("Color Scheme")]
    public Color primaryColor;
    public Color primaryColor_1;
    public Color primaryColor_2;
    public Color primaryColor_3;

    [Space(10)]

    public Color secondaryColor;
    public Color secondaryColor_1;
    public Color secondaryColor_2;
    public Color secondaryColor_3;

    [Space(10)]

    public Color successColor;
    public Color errorColor;
    public Color warningColor;

    [Space(10)]

    public Color whiteColor;
    public Color lightgreyColor;
    public Color greyColor;
    public Color darkgreyColor;

    [Space(10)]

    [Header("Titles Color Scheme")]

    public Color capgeminiBlue;
    public Color vibrantBlue;
    public Color deepPurple;
    public Color techRed;
    public Color zestGreen;



    [Space(10)]

    [Header("Font Scheme")]
    public TMP_FontAsset Font_L;
    public TMP_FontAsset Font_R;
    public TMP_FontAsset Font_M;
    public TMP_FontAsset Font_B;

    [Space(10)]

    public int H1_size;
    public int H2_size;
    public int H3_size;
    public int H4_size;

    [Space(10)]

    public int Subtitle_size;
    public int Controltext_size;

    [Space(10)]

    public int Body_size;
    public int Body_lineheight;

    [Header("Shadow")]
    public Color effectColor;
    public Vector2 Shadow;

    [Space(10)]

    [Header("AR Button")]
    public Sprite buttonSprite;
    public SpriteState buttonSpriteState;

    [Space(10)]

    public Sprite buttonRoundSprite;
    public SpriteState buttonRoundSpriteState;

    [Space(10)]

    [Header("AR AlertIcon")]
    public Sprite confirmSprite;
    public Sprite declineSprite;
    public Sprite warningSprite;


    [Header("AR Text")]
    public string Text;
}
