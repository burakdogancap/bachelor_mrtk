﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Font_AR_DesignSystem : AR_DesignSystem
{

    //Elements to style
    TextMeshProUGUI text;

    //Component Type Options
    public enum FontType
    {
        //Types of Fonts
        H1,
        H2,
        H3,
        H4,

        Body,
        Subtitle,
        Controltext,
    }

    //Choose FontType
    public FontType fontType;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        text = GetComponent<TextMeshProUGUI>();

        //Define Style of FontTypes
        switch (fontType)
        {
            case FontType.H1:
                text.font = skinData.Font_B;
                text.fontSize = skinData.H1_size;
                break;
            case FontType.H2:
                text.font = skinData.Font_R;
                text.fontSize = skinData.H2_size;         
                break;
            case FontType.H3:
                text.font = skinData.Font_R;
                text.fontSize = skinData.H3_size;
                break;
            case FontType.H4:
                text.font = skinData.Font_B;
                text.fontSize = skinData.H4_size;
                break;
            case FontType.Body:
                text.font = skinData.Font_R;
                text.fontSize = skinData.Body_size;
                break;
            case FontType.Subtitle:
                text.font = skinData.Font_L;
                text.fontSize = skinData.Subtitle_size;
                break;
            case FontType.Controltext:
                text.font = skinData.Font_R;
                text.fontSize = skinData.Controltext_size;
                break;
        }
    }
}
