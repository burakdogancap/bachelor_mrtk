﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Experimental.U2D;
using UnityEngine.Experimental.Rendering;

public class AR_DesignSystem_RadioButton : AR_DesignSystem
{
    [SerializeField] private GameObject Background = null;
    [SerializeField] private GameObject CheckMark = null;

    public enum RadioButtonType
    {
        //Types of SelectionControls for Switch
        Default,
        Confirm,
        Decline,
        Warning
    }

    Image background;
    Image checkmark;

    public RadioButtonType radioButtonType;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        background = Background.GetComponent<Image>();
        checkmark = CheckMark.GetComponent<Image>(); ;

        switch (radioButtonType)
        {
            case RadioButtonType.Default:
                background.color = skinData.primaryColor;
                checkmark.color = skinData.primaryColor;
                break;
            case RadioButtonType.Confirm:
                background.color = skinData.successColor;
                checkmark.color = skinData.successColor;
                break;
            case RadioButtonType.Decline:
                background.color = skinData.errorColor;
                checkmark.color = skinData.errorColor;
                break;
            case RadioButtonType.Warning:
                background.color = skinData.warningColor;
                checkmark.color = skinData.warningColor;
                break;
        }
    }
}
