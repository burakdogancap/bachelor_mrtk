﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Experimental.U2D;
using UnityEngine.Experimental.Rendering;


public class AR_Design_Card : AR_DesignSystem
{
    [SerializeField] private GameObject ImagePanel = null;
    [SerializeField] private GameObject TextPanel = null;
    [SerializeField] private GameObject TextPanel_CardTitel = null;
    [SerializeField] private GameObject TextPanel_CardDesk = null;


    public enum CardsType
    {
        //Types of Cards
        WithImage,
        WithoutImage
    }
    public enum CardsColor
    {
        //Types of CardsColor
        Default,
        DefaultAlt
    }

    Image imageTextPanel;
    TextMeshProUGUI cardTitle;
    TextMeshProUGUI cardDesc;

    public CardsType cardType;
    public CardsColor cardColor;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        imageTextPanel = TextPanel.GetComponent<Image>();
        cardTitle = TextPanel_CardTitel.GetComponent<TextMeshProUGUI>();
        cardDesc = TextPanel_CardDesk.GetComponent<TextMeshProUGUI>();


        switch (cardType)
        {
            case CardsType.WithImage:
                ImagePanel.SetActive(true);
                break;
            case CardsType.WithoutImage:
                ImagePanel.SetActive(false);
                break;
        }

        switch (cardColor)
        {
            case CardsColor.Default:
                imageTextPanel.color = skinData.whiteColor;
                cardTitle.color = skinData.primaryColor;
                cardDesc.color = skinData.primaryColor;
                break;
            case CardsColor.DefaultAlt:
                imageTextPanel.color = skinData.primaryColor;
                cardTitle.color = skinData.whiteColor;
                cardDesc.color = skinData.whiteColor;
                break;
        }
    }
}
