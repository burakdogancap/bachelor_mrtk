﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AR_DesignSystem_InstanceFont : Editor
{
    //Adding Fonts
    [MenuItem("GameObject/AR DesignSystem_Fonts/H1", priority = 0)]
    public static void AddH1()
    {
        Create("H1-Titel");
    }

    static GameObject clickedObject;

    private static GameObject Create(string objectName)
    {
        GameObject instance = Instantiate(Resources.Load<GameObject>("UI_Fonts/" + objectName));
        instance.name = objectName;
        clickedObject = UnityEditor.Selection.activeObject as GameObject;
        {
            instance.transform.SetParent(clickedObject.transform, false);
        }
        return instance;
    }
}
