﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AR_DesignSystem_Instance : Editor
{
    /*Allows to add Components directly over editor*/
    [MenuItem("GameObject/AR DesignSystem/Button", priority = 0)]
    public static void AddButton()
    {
        Create("Button");
    }

    [MenuItem("GameObject/AR DesignSystem/Tooltip", priority = 0)]
    public static void AddTooltip()
    {
        Create("Tooltip");
    }

    [MenuItem("GameObject/AR DesignSystem/Checkbox", priority = 0)]
    public static void AddCheckbox()
    {
        Create("Checkbox");
    }

    [MenuItem("GameObject/AR DesignSystem/RadioButton", priority = 0)]
    public static void AddRadioButton()
    {
        Create("RadioButton");
    }

    [MenuItem("GameObject/AR DesignSystem/Card", priority = 0)]
    public static void AddCard()
    {
        Create("Card");
    }

    [MenuItem("GameObject/AR DesignSystem/Dialog", priority = 0)]
    public static void AddDialog()
    {
        Create("Dialog");
    }

    static GameObject clickedObject;

    private static GameObject Create(string objectName)
    {
        GameObject instance = Instantiate(Resources.Load<GameObject>("UI_Components/" + objectName));
        instance.name = objectName;
        clickedObject = UnityEditor.Selection.activeObject as GameObject;
        {
            instance.transform.SetParent(clickedObject.transform, false);
        }
        return instance;
    }
}
